# -*- coding:utf-8 -*-
import yaml
import os
import json

#获取yaml文件，返回字典
def getDict(yaml_path):
    try:
        yaml_file = open(yaml_path,encoding='utf-8')
        yaml_json = yaml.load(yaml_file,Loader=yaml.FullLoader)
        return yaml_json
    except FileNotFoundError:
        print('指定的配置文件%s,不存在，请检查配置文件。'%yaml_path)



#print(getDict(r'config\test.yml'))