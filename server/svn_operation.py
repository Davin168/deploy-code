# -*- coding:utf-8 -*-
import os

def svnCheckout(url,local_path,username,password):
    cmd = "svn co %s %s --username %s --password %s --non-interactive"%(url,local_path,username,password)
    cmd_info = os.system(cmd)
    print(cmd_info)
    if str(cmd_info) != "0":
        return("当前系统为安装svn命令，请安装svn：yum install subversion")
    else:
        return cmd_info


def svnUpdate(local_path,username,password):
    cmd = "svn update --username %s --password %s %s"%(username,password,local_path)
    cmd_info = os.system(cmd)
    print(cmd_info)
    if str(cmd_info) != "0":
        return("错误：svn update %s 命令执行失败!"%local_path)
    else:
        return cmd_info

def svnCommit(local_path,username,password,version):
    cmd = "svn update -r %s --username %s --password %s %s"%(version,username,password,local_path)
    cmd_info = os.system(cmd)
    print(cmd_info)
    if str(cmd_info) != "0":
        return("错误：svn update -r %s --username xxx --password xxx %s 命令执行失败!"%(version,local_path))
    else:
        return cmd_info

if __name__ == '__main__':
    #svn拉取
    #print(svnCheckout('svn://xxxxxx:8989/yunwei', '/root/test_svn','cxxx','Dxxxx'))
    #svn更新
    #print(svnUpdate('/root/test_svn','xxxxx','xxxx'))
    #svn更新到指定版本
    print(svnCommit('/root/test_svn','xxxx','xxxxxx','530'))