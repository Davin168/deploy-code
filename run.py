# -*- coding:utf-8 -*-
from server.argparse_operation import getParameters
from server.issued_rollback import PostCode
import os
import sys

def judgeInstruction():
    '''
    判断用户发布或回滚
    '''
    os.chdir(sys.path[0])   #切换到run.py的工作目录
    input_parameters = PostCode(getParameters())
    release_choose = os.environ.get('status')
    release_commit = os.environ.get('commit')
    print('当前status的环境变量是：%s'%release_choose)
    print('当前commit的环境变量是：%s'%release_commit)
    #release_choose = "Rollback"
    #release_commit = "lkjkljsladjfoiuawennvljlaj5456468"


    if release_choose == 'Deploy':
        print('---------------准备执行发布流程----------------')
        if release_commit == None:
            input_parameters.deployCode()
        else:
            sys.exit('错误：发布无需填入commit参数。')

    elif release_choose == 'Rollback':
        print('---------------准备执行回滚流程----------------')
        if release_commit == None:
            sys.exit('错误：回滚的commit参数为空，改值为必填项。')
        input_parameters.rollbackCode(commit_num=release_commit)

    elif release_choose == 'LastVersion':
        print('---------------准备执行回滚上一个版本流程----------------')
        if release_commit == None:
            input_parameters.lastVersionCode()
        else:
            sys.exit('错误：发布无需填入commit参数。')

    else:
        sys.exit('用户选择Deploy/Rollback的环境变量异常')
    



if __name__ == '__main__':
    judgeInstruction()
