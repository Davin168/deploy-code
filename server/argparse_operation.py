# -*- coding:utf-8 -*-
import argparse

def getParameters():
    parser = argparse.ArgumentParser(description='Calculate volume of a cylinder')
    parser.add_argument('--config', help='指定yaml文件名称')
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    print(getParameters())
