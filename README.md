# DeployCode

##### 介绍

运维发布系统，通过制定yaml，对业务进行特殊发布配置。



发布具备以下三个功能：

- Deploy      #发布，commit要为空
- Rollback    #回滚指定commit，commit不能为空
- LastVersion   #回滚到上一个版本，commit要为空



##### 架构

![1648452130258](image/1.png)



##### 准备环境

**环境要求**

ansible == 2.11.0或以上版本



**安装ansible**

使用pip的蟒蛇的环境中安装该需求包己选择的当前用户： 

```
python3 -m pip install --user ansible
```

或者，你也可以安装一个具体版本的  `ansible-core` 在这蟒蛇的环境： 

```
python3 -m pip install --user ansible-core==2.11.9
```

**升级环境**

要升级现有需求的安装在这个Python环境的最新发布的版本，简单地添加  `--upgrade` 到上面的命令: 

```
python3 -m pip install --upgrade --user ansible
```

**确认安装**

你可以试，你是正确安装通过检查的版本： 

```
ansible --version
```



发布系统调用ansible的api，默认读取/etc/ansible/hosts文件，hosts文件配置如下

```shell
[api]   #群组
192.168.0.5 ansible_ssh_host=192.168.0.5 ansible_ssh_port=22 ansible_ssh_user=root ansible_connection=ssh
192.168.0.6 ansible_ssh_host=192.168.0.6 ansible_ssh_port=22 ansible_ssh_user=root ansible_connection=ssh
#别名   ansible_ssh_host=内网/公网ip ansible_ssh_port=端口 ansible_ssh_user=连接用户 ansible_connection=连接方式
```

配置hosts文件时，主机的别名必须是内网ip或者外网ip，因为发布过程中调用了ansible的synchronize模块，该模块有缺陷，如别名不写ip地址，会报错，提示找不到dns解析。



##### 使用教程

安装python模块

```shell
pip3 install -r requirements.txt
```



根据业务情况创建yaml文件，将yaml文件放入config处，随意命名

###### git示例

config/spug.yml  ，案例如下

```yaml
Git:
  GitPath: 'code/'
  GitUrl: 'ssh://git@xxxx/yunwei/spug.git'
  GitBranch: 'test'

Web:
  fast_deploy: False
  webroot: '/data/test'     
  keep_version: 5
  version_retain_path: '/data/CodeVersion'
  exclude:
    - ".git"
    - ".svn"
    - "README.md"
    - "456.txt"
    - "docs/123.txt"

Ansible:
  group: 'log'
  check_out _before: 
    - module: 'command'
      args: 'ls /data'
    - module: 'shell'
      args: 'uptime'

  after_check_out: 
    - module: 'command'
      args: 'ls /data'
    - module: 'shell'
      args: 'uptime'
  
  before_code_replacement: 
    - module: 'command'
      args: 'ls /data'
    - module: 'shell'
      args: 'uptime'


  after_code_replacement:
    - module: 'command'
      args: 'ls /data'
    - module: 'shell'
      args: 'uptime'

```

配置说明

```yaml
Git:					#git配置项
  GitPath: 'code/'  #存放在本地的哪个路径下，建议给相对路径存放在发布项目的code目录下code/
  GitUrl: 'ssh://git@gitlab.xxxx.cn/yunwei/spug.git'  #git项目地址，前提做好免密拉取
  GitBranch: 'test'	#拉取的项目分支

Web:	#目标主机配置项
  fast_deploy: False     #是否开启快速发布，True开启，False关闭，建议：测试环境开启
  webroot: '/data/test'     #存放在目标机上的哪个目录下，/data/project
  kee_version: 5            #指定在目标机器上保留几个版本。
  version_retain_path: '/data/CodeVersion'       #传送到代码指定路径下如supg，进行备份在服务器上不进行版本备份，该备份是用于批量切换上线代码
  exclude:				#过滤指定目录或文件 (- "文件"/- "目录")
    - ".git"
    - ".svn"
    - "README.md"
    - "456.txt"
    - "docs/123.txt"

Ansible:			#ansible配置项
  group: 'log'		#发布的主机组，在ansible中的/etc/ansible/hosts配置好
  #检出代码前操作(本地发布服务器上执行)
  check_out _before:  #如不需要执行操作请赋值null
      #指定ansible模块
    - module: 'command'
      #模块的执行参数
      args: 'ls /data'
    - module: 'shell'
      args: 'uptime'

  #检出代码后操作(本地发布服务器执行)
  after_check_out: #可赋值null（空，不执行任何操作）
    - module: 'command'
      args: 'ls /data'
    - module: 'shell'
      args: 'uptime'
  
  #代码替换前操作（目标服务器执行）
  before_code_replacement: #可赋值null（空，不执行任何操作）
    - module: 'command'
      args: 'ls /data'
    - module: 'shell'
      args: 'uptime'


  #代码替换后操作（目标服务器执行）
  after_code_replacement: null  #赋值null（空，不执行任何操作）
```

###### svn示例

config/spug.yml  ，案例如下

```yaml
Svn:
  SvnPath: 'code/'
  SvnUrl: 'svn://xxxxxxx/porject'
  SvnUser: 'xxxx'
  SvnPass: 'xxxxxxxx'

Web:
  fast_deploy: False
  webroot: '/data/test'     
  keep_version: 5
  version_retain_path: '/data/CodeVersion_svn'
  exclude:
    - ".git"
    - ".svn"
    - "README.md"
    - "456.txt"
    - "docs/123.txt"

Ansible:
  group: 'log'
  check_out _before: 
    - module: 'command'
      args: 'ls /data'
    - module: 'shell'
      args: 'uptime'

  after_check_out: 
    - module: 'command'
      args: 'ls /data'
    - module: 'shell'
      args: 'uptime'
  
  before_code_replacement: 
    - module: 'command'
      args: 'ls /data'
    - module: 'shell'
      args: 'uptime'


  after_code_replacement:
    - module: 'command'
      args: 'ls /data'
    - module: 'shell'
      args: 'uptime'

```

配置说明

```yaml
Svn:	#svn配置项
  SvnPath: 'code/'  #存放在本地的哪个路径下，建议给相对路径存放在发布项目的code目录下code/
  SvnUrl: 'svn://xxxxxxx/porject'  #svn项目地址
  SvnUser: 'xxxx'   #svn账户名
  SvnPass: 'xxxxxxxx'   #svn密码

Web:	#目标主机配置项
  fast_deploy: False     #是否开启快速发布，True开启，False关闭，建议：测试环境开启
  webroot: '/data/test'     #存放在目标机上的哪个目录下，/data/project
  kee_version: 5            #指定在目标机器上保留几个版本。
  version_retain_path: '/data/CodeVersion'       #传送到代码指定路径下如supg，进行备份在服务器上不进行版本备份，该备份是用于批量切换上线代码
  exclude:				#过滤指定目录或文件 (- "文件"/- "目录")
    - ".git"
    - ".svn"
    - "README.md"
    - "456.txt"
    - "docs/123.txt"

Ansible:			#ansible配置项
  group: 'log'		#发布的主机组，在ansible中的/etc/ansible/hosts配置好
  #检出代码前操作(本地发布服务器上执行)
  check_out _before:  #如不需要执行操作请赋值null
      #指定ansible模块
    - module: 'command'
      #模块的执行参数
      args: 'ls /data'
    - module: 'shell'
      args: 'uptime'

  #检出代码后操作(本地发布服务器执行)
  after_check_out: #可赋值null（空，不执行任何操作）
    - module: 'command'
      args: 'ls /data'
    - module: 'shell'
      args: 'uptime'
  
  #代码替换前操作（目标服务器执行）
  before_code_replacement: #可赋值null（空，不执行任何操作）
    - module: 'command'
      args: 'ls /data'
    - module: 'shell'
      args: 'uptime'


  #代码替换后操作（目标服务器执行）
  after_code_replacement: null  #赋值null（空，不执行任何操作）
```

###### 

##### 配置jenkins

参数化构建过程

![1648453456635](image/2.png)

构建shell

![1648453466635](image/3.png)



##### 执行

需要配置jenkins才可执行次命令，因为run.py会获取环境变量，该环境变量是由jenkins提供

--config定义你写好的配置文件，发布系统会根据你写好的yaml进行发布应用。

```shell
python3  run.py --config "spug.yml"
```

# 

##### 简易发布

开启简易发布请修改配置文件yaml

```yaml
Web:
  fast_deploy: True
```

优点：超快速部署

缺点：无法回滚上一版本

建议：一般测试服和灰度服务器才开启快速部署的功能

